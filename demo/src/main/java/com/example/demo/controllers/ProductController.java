package com.example.demo.controllers;

import com.example.demo.Models.Product;
import com.example.demo.Models.ResponseObject;
import com.example.demo.repositories.ProductRepositories;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController //khai bao day la controllers
@RequestMapping(path = "/api/v1/Products") // khai bao duong link de tro toi file controller nay
public class ProductController {

    @Autowired //dung cac ham bean method
    private ProductRepositories repositories;

    //    @GetMapping("getAllProducts") // khai bao tro toi action trong controller
//    List<Product> getAllProducts() {
//        return List.of(
//                new Product(1L,"iphone",2002,200.2,""),
//                new Product(2L,"iphone2",2001,200.3,"")
//        );
//    }
    @GetMapping("getAllProducts")
    // khai bao tro toi action trong controller
    List<Product> getAllProducts() {
        return repositories.findAll();
    }

    @GetMapping("getProduct/{id}")
//        // khai bao tro toi action trong controller
//    Optional<Product> findById(@PathVariable Long id) { //optional tra ve ca kieu null
//        return repositories.findById(id);
//    }
    ResponseEntity<ResponseObject> findById(@PathVariable Long id) { // custom messenger tra ve
        Optional<Product> product = repositories.findById(id);
        return product.isPresent() ?
                ResponseEntity.status(HttpStatus.OK).body(
                        new ResponseObject("ok", "Query product success !!!", product)
                ) :
                ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        new ResponseObject("False", "Cannot find product with id = " + id, "")
                );
    }

    @PostMapping("insertProduct")
    ResponseEntity<ResponseObject> insertProduct(@RequestBody @NotNull Product newProduct) { // lay cac gia tri tu body gui len
        List<Product> product = repositories.findByName(newProduct.getName().trim());
        if (product.size() > 0) {
            return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(
                    new ResponseObject("failed", "Product name already taken", "")
            );
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("ok", "Insert Product Successfully", repositories.save(newProduct))
            );
        }
    }

    @PutMapping("updateProduct/{id}")
    ResponseEntity<ResponseObject> updateProduct(@RequestBody Product newProduct,@PathVariable Long id) {
        Product updateProduct = repositories.findById(id)
                .map(product -> {
                    product.setName(newProduct.getName());
                    product.setYear(newProduct.getYear());
                    product.setPrice(newProduct.getPrice());
                    product.setUrl(newProduct.getUrl());
                    return repositories.save(product);

                }).orElseGet(() -> {
                    newProduct.setId(id);
                    return repositories.save(newProduct);
                });
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("ok","Update Product Successfully",updateProduct)
        );
    }
    @DeleteMapping("delete/{id}")
    ResponseEntity<ResponseObject> deleteProduct(@PathVariable Long id) {
        boolean exits = repositories.existsById(id);
        if (exits) {
            repositories.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("ok","Delete Product Successfully","")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("failed","Cannot Delete Product Because Not Find Product Id = " + id,"")
        );
    }
}
