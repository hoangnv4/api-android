package com.example.demo.controllers;

import com.example.demo.Models.ResponseObject;
import com.example.demo.Models.User;
import com.example.demo.repositories.UserRepositories;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/user")
public class UserController {
    @Autowired //dung cac ham bean method
    private UserRepositories userRepositories;
    @PostMapping("login")
    ResponseEntity<ResponseObject> userLogin(@RequestBody @NotNull User user) {
        User userDb = userRepositories.findByName(user.getName().trim());
        if (userDb != null) {
            if(user.getPassword().equals(userDb.getPassword())) {
                return ResponseEntity.status(HttpStatus.OK).body(
                        new ResponseObject("success", "login success", userDb)
                );
            }
            else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        new ResponseObject("fail", "wrong password", "")
                );
            }
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject("fail", "wrong username", "")
            );
        }
    }
}
