package com.example.demo.controllers;

import com.example.demo.Models.News;
import com.example.demo.Models.ResponseObject;
import com.example.demo.repositories.NewRepositories;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/v1/news")
public class NewController {

    @Autowired
    private NewRepositories newRepositories;

    @GetMapping("getAllNews")
        // khai bao tro toi action trong controller
    List<News> getAllNews() {
        return newRepositories.findAll();
    }

    @PostMapping("insertNew")
    ResponseEntity<ResponseObject> insertNew(@RequestBody @NotNull News tintuc) { // lay cac gia tri tu body gui len
        List<News> news = newRepositories.findByTitle(tintuc.getTitle().trim());
        if (news.size() > 0) {
            return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(
                    new ResponseObject("failed", "News name already taken", "")
            );
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("ok", "Insert New Successfully", newRepositories.save(tintuc))
            );
        }
    }

    @PutMapping("updateNew/{id}")
    ResponseEntity<ResponseObject> updateNew(@RequestBody News tintucmoi, @PathVariable Long id) {
        Optional<News> tintuccu = newRepositories.findById(id);
        if (tintuccu.isPresent()) {
            tintuccu.map(news -> {
                news.setTitle(tintucmoi.getTitle());
                news.setDescription(tintucmoi.getDescription());
                news.setLink_img(tintucmoi.getLink_img());
                news.setType(tintucmoi.getType());
                return newRepositories.save(news);
            });
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject("False", "Cannot find new with id = " + id, "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("OK", "Update new success", tintuccu)
        );
    }

    @DeleteMapping("delete/{id}")
    ResponseEntity<ResponseObject> deleteNew(@PathVariable Long id) {
        boolean exits = newRepositories.existsById(id);
        if (exits) {
            newRepositories.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("ok", "Delete New Successfully", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("failed", "Cannot Delete New Because Not Find New Id = " + id, "")
        );
    }

    @GetMapping("typeNew/{type}")
    ResponseEntity<ResponseObject> findByTypeNew(@PathVariable Integer type) { // custom messenger tra ve
        List<News> news = newRepositories.findByType(type);
        return news.size() > 0 ?
                ResponseEntity.status(HttpStatus.OK).body(
                        new ResponseObject("ok", "Query news success !!!", news)
                ) :
                ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        new ResponseObject("False", "Cannot find news with type new = " + type, "")
                );
    }

//    @GetMapping("dateBetwwen")
//    ResponseEntity<ResponseObject> findNewBetweenDate () {
//        List<News> news = newRepositories.findByCreatedBetween( Date.valueOf("2022-05-10"), Date.valueOf("2022-05-12"));
//        return news.size() > 0 ?
//                ResponseEntity.status(HttpStatus.OK).body(
//                        new ResponseObject("ok", "Query news success !!!", news)
//                ) :
//                ResponseEntity.status(HttpStatus.NOT_FOUND).body(
//                        new ResponseObject("False", "Cannot find news", "")
//                );
//    }
}
