package com.example.demo.controllers;

import com.example.demo.Models.News;
import com.example.demo.Models.Product;
import com.example.demo.Models.ResponseObject;
import com.example.demo.Models.TypeNew;
import com.example.demo.repositories.TypeNewRepositories;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController //khai bao day la controllers
@RequestMapping(path = "/api/v1/typeNew") // khai bao duong link de tro toi file controller nay
public class TypeNewController {
    @Autowired
    private TypeNewRepositories typeNewRepositories;
    @GetMapping("getAllTypeNew")
        // khai bao tro toi action trong controller
    List<TypeNew> getAllTypeNew() {
        return typeNewRepositories.findAll();
    }

    @PostMapping("insertTypeNew")
    ResponseEntity<ResponseObject> insertTypeNew(@RequestBody @NotNull TypeNew kieumoi) { // lay cac gia tri tu body gui len
        List<TypeNew> typeNew = typeNewRepositories.findByName(kieumoi.getName().trim());
        if (typeNew.size() > 0) {
            return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(
                    new ResponseObject("failed", "Type New name already taken", "")
            );
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("ok", "Insert Type New Successfully", typeNewRepositories.save(kieumoi))
            );
        }
    }

    @PutMapping("updateTypeNew/{id}")
    ResponseEntity<ResponseObject> updateTypeNew(@RequestBody TypeNew kieutin, @PathVariable Long id) {
        Optional<TypeNew> kieutincu = typeNewRepositories.findById(id);
        if (kieutincu.isPresent()) {
            kieutincu.map(news -> {
                news.setName(kieutin.getName());
                news.setDescription(kieutin.getDescription());
                return typeNewRepositories.save(news);
            });
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject("False", "Cannot find type new with id = " + id, "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("OK", "Update type new success", kieutincu)
        );
    }

    @DeleteMapping("delete/{id}")
    ResponseEntity<ResponseObject> deleteTypeNew(@PathVariable Long id) {
        boolean exits = typeNewRepositories.existsById(id);
        if (exits) {
            typeNewRepositories.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("ok", "Delete Type New Successfully", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("failed", "Cannot Delete Type New Because Not Find Type New Id = " + id, "")
        );
    }
}
