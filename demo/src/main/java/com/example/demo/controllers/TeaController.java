package com.example.demo.controllers;

import com.example.demo.Models.ResponseObject;
import com.example.demo.Models.Tea;
import com.example.demo.repositories.TeaRepositories;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/tea")
public class TeaController {
    @Autowired //dung cac ham bean method
    private TeaRepositories teaRepositories;

    @GetMapping("getAllTea")
    // khai bao tro toi action trong controller
    public List<Tea> getAllTea() {
        return teaRepositories.findAll();
    }

    @GetMapping("getTea/{name}")
    ResponseEntity<ResponseObject> findById(@PathVariable String name) { // custom messenger tra ve
        List<Tea> tea = teaRepositories.findByName(name);
        return tea.size() > 0 ?
                ResponseEntity.status(HttpStatus.OK).body(
                        new ResponseObject("ok", "Query tea success !!!", tea)
                ) :
                ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        new ResponseObject("False", "Cannot find tea with name = " + name, "")
                );
    }

    @PostMapping("insertTea")
    ResponseEntity<ResponseObject> insertTea(@RequestBody @NotNull Tea newTea) { // lay cac gia tri tu body gui len
        List<Tea> tea = teaRepositories.findByName(newTea.getName().trim());
        if (tea.size() > 0) {
            return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(
                    new ResponseObject("failed", "Tea name already taken", "")
            );
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("ok", "Insert Tea Successfully", teaRepositories.save(newTea))
            );
        }
    }

    @PutMapping("updateTea/{id}")
    ResponseEntity<ResponseObject> updateTea(@RequestBody Tea newTea, @PathVariable Long id) {
        Tea updateTea = teaRepositories.findById(id)
                .map(tea -> {
                    tea.setName(newTea.getName());
                    tea.setPrice(newTea.getPrice());
                    tea.setLink_img("src/image_tea/tra8.jpg");
                    tea.setType_tea(newTea.getType_tea());
                    tea.setInfomation(newTea.getInfomation());
                    return teaRepositories.save(tea);
                }).orElseGet(() -> {
                    newTea.setId(id);
                    return teaRepositories.save(newTea);
                });
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("ok", "Update Tea Successfully", updateTea)
        );
    }

    @DeleteMapping("delete/{id}")
    ResponseEntity<ResponseObject> deleteTea(@PathVariable Long id) {
        boolean exits = teaRepositories.existsById(id);
        if (exits) {
            teaRepositories.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("ok", "Delete Tea Successfully", "")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("failed", "Cannot Delete Tea Because Not Find Tea Id = " + id, "")
        );
    }

}
