package com.example.demo.controllers;

import com.example.demo.Models.Orders;
import com.example.demo.Models.ResponseObject;
import com.example.demo.repositories.OrderRepositories;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/order")
public class OrderController {
    @Autowired
    private OrderRepositories orderRepositories;

    @GetMapping("getAllOrder")
        // khai bao tro toi action trong controller
    public List<Orders> getAllOrder() {
        return orderRepositories.findAll();
    }

    @PostMapping("insertOrder")
    ResponseEntity<ResponseObject> insertProduct(@RequestBody @NotNull Orders newOrder) {

        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("ok", "Insert Order Successfully", orderRepositories.save(newOrder))
        );
    }
}
