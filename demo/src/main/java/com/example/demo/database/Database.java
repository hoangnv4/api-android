package com.example.demo.database;

import com.example.demo.Models.Product;
import com.example.demo.Models.News;
import com.example.demo.Models.TypeNew;
import com.example.demo.Models.User;
import com.example.demo.repositories.NewRepositories;
import com.example.demo.repositories.TypeNewRepositories;
import com.example.demo.repositories.UserRepositories;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.Date;


@Configuration
public class Database {
    private static final Logger logger = LoggerFactory.getLogger(Database.class); // dung de khai bao log cho class nao

    @Bean // chay ngay khi app duoc tao
    CommandLineRunner initnewDatabase(NewRepositories newRepositories) {
        return new CommandLineRunner() {
            @Override
            public void run(String... args) throws Exception {
//                String name, Double price, String link_img, Integer type_new, String infomation
                News new1 = new News("Thường trực Ban Bí thư Võ Văn Thưởng: Phát hiện tham nhũng tới đâu, xử tới đó!","Ngày 13-5, tại buổi tiếp xúc cử tri huyện Hòa Vang, TP Đà Nẵng, Ủy viên Bộ Chính trị, Thường trực Ban Bí thư Võ Văn Thưởng đã nói về công tác phòng chống tham nhũng.","src/image_new/tra1.jpg",1, Date.valueOf("2022-05-10"));
                News new2 = new News("Ngày 15/5: Tin vui chống dịch, ca COVID-19 mới giảm, số khỏi bệnh nhiều gấp 3 số mắc, không có F0 tử vong","SKĐS - Bản tin phòng chống dịch COVID-19 ngày 15/5 của Bộ Y tế cho biết có 1.594 ca mắc mới COVID-19 tại 47 tỉnh, thành, giảm so với hôm qua; số khỏi bệnh nhiều gấp 3 lần số mắc mới; đây là lần thứ 4 trong thời gian gần đây không có bệnh nhân COVID-19 tử vong trong ngày.","src/image_new/tra2.jpg",1,Date.valueOf("2022-05-11"));
                News new3 = new News("Tuyển điền kinh Việt Nam liên tiếp giành HCV","Chiều 15/5, Nguyễn Thị Oanh cán đích đầu tiên ở nội dung 3.000 m vượt chướng ngại vật nữ với thời gian 9 phút 52 giây 46.","",2,Date.valueOf("2022-05-12"));
                News new4 = new News("Thí điểm khai cấp hộ chiếu phổ thông qua cổng dịch vụ công","Khi sử dụng dịch vụ công trực tuyến mức độ 4, người dân sẽ giảm thiểu số lần đi lại, thời gian chờ đợi nộp hồ sơ, nhận kết quả.","",2,Date.valueOf("2022-05-13"));
                News new5 = new News("Thắng đậm U23 Myanmar, U23 Indonesia tạm chiếm ngôi đầu của U23 Việt Nam","Thắng đậm U23 Myanmar, U23 Indonesia tạm chiếm ngôi đầu của U23 Việt Nam","",3,Date.valueOf("2022-05-14"));
                News new6 = new News("Đánh bom liều chết gần biên giới Pakistan - Afghanistan","Ngày 15/5, Pakistan thông báo ít nhất 6 người thiệt mạng trong một vụ đánh bom liều chết nhằm vào xe quân sự ở vùng Tây Bắc, giáp biên giới Afghanistan.","",3,Date.valueOf("2022-05-15"));
                logger.info("insert data new : " + newRepositories.save(new1));
                logger.info("insert data new: " + newRepositories.save(new2));
                logger.info("insert data new: " + newRepositories.save(new3));
                logger.info("insert data new: " + newRepositories.save(new4));
                logger.info("insert data new: " + newRepositories.save(new5));
                logger.info("insert data new: " + newRepositories.save(new6));

            }
        };
    }

    @Bean // chay ngay khi app duoc tao
    CommandLineRunner initUserDatabase(UserRepositories userRepositories) {
        return new CommandLineRunner() {
            @Override
            public void run(String... args) throws Exception {
                User user1 = new User("hoangnv","hoangnv@gmail.com","hoang123@","admin");
                User user2 = new User("trangnt","trangnt@gmail.com","trang123@","client");

                logger.info("insert data user: " + userRepositories.save(user1));
                logger.info("insert data user: " + userRepositories.save(user2));
            }
        };
    }

    @Bean // chay ngay khi app duoc tao
    CommandLineRunner iniTypeDatabase(TypeNewRepositories typeNewRepositories) {
        return new CommandLineRunner() {
            @Override
            public void run(String... args) throws Exception {
                TypeNew typeNew1 = new TypeNew("type one","day la type one");
                TypeNew typeNew2 = new TypeNew("type two","day la type two");

                logger.info("insert data TypeNew: " + typeNewRepositories.save(typeNew1));
                logger.info("insert data TypeNew: " + typeNewRepositories.save(typeNew2));
            }
        };
    }
}
