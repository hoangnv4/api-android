package com.example.demo.Models;

import javax.persistence.*;

@Entity // dung de khai bao khoa chinh

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)  // dung de khai bao khoa chinh tu sinh
    private Long id;
    @Column(nullable = false,unique = true,length = 45) //dung de validate thuoc tinh cho truong name
    private String name;
//    @Column(nullable = false,unique = true) //dung de validate thuoc tinh cho truong name

    private String email;

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Column(nullable = false)
    private String password;

    public User() {
    }

    public User(String name, String email, String password, String type) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.type = type;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
