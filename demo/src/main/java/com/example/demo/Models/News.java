package com.example.demo.Models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity // dung de khai bao khoa chinh

public class News {
    @Id  // dung de khai bao khoa chinh
    @GeneratedValue(strategy = GenerationType.AUTO)  // dung de khai bao khoa chinh tu sinh
    private Long id;
    @Column(nullable = false,unique = true,length = 255) //dung de validate thuoc tinh cho truong name
    private String title;

    @Column(nullable = false,unique = true,length = 1000000) //dung de validate thuoc tinh cho truong name
    private String description;
    private String link_img;

    private Integer type;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date created;

//    private Date from;
//    private Date to;

    public News() {
    }

    public News( String title, String description, String link_img, Integer type, Date created) {
        this.title = title;
        this.description = description;
        this.link_img = link_img;
        this.type = type;
        this.created = created;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink_img() {
        return link_img;
    }

    public void setLink_img(String link_img) {
        this.link_img = link_img;
    }

    public Date getcreated() {
        return created;
    }

    public void setcreated(Date created) {
        this.created = created;
    }
}
