package com.example.demo.Models;

public class ResponseObject {
    private String status;
    private String messenger;
    private Object data;

    public ResponseObject(String status, String messenger, Object data) {
        this.status = status;
        this.messenger = messenger;
        this.data = data;
    }

    public ResponseObject() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessenger() {
        return messenger;
    }

    public void setMessenger(String messenger) {
        this.messenger = messenger;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
