package com.example.demo.Models;

import javax.persistence.*;
import java.util.Calendar;

@Entity // dung de khai bao khoa chinh
@Table(name = "tblProduct") //dung de khai bao dung bang nao trong db
public class Product {
    @Id  // dung de khai bao khoa chinh
    @GeneratedValue(strategy = GenerationType.AUTO)  // dung de khai bao khoa chinh tu sinh
    private Long id;
    @Column(nullable = false,unique = true,length = 255) //dung de validate thuoc tinh cho truong name
    private String name;
    private int year;
    private Double price;
    private String url ;
    @Transient // dung de khai bao mot truong khong co trong db
    private int age;
    public int getAge() {
        return Calendar.getInstance().get(Calendar.YEAR) - year; //lay gia tri cua nam hien tai  Calendar.getInstance().get(Calendar.YEAR)
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    public Product() {

    }

    public Product(String name, int year, Double price, String url) {
        this.name = name;
        this.year = year;
        this.price = price;
        this.url = url;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", year=" + year +
                ", price=" + price +
                ", url='" + url + '\'' +
                '}';
    }

}
