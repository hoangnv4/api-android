package com.example.demo.Models;

import javax.persistence.*;

@Entity // dung de khai bao khoa chinh

public class Tea {
    @Id  // dung de khai bao khoa chinh
    @GeneratedValue(strategy = GenerationType.AUTO)  // dung de khai bao khoa chinh tu sinh
    private Long id;
    @Column(nullable = false,unique = true,length = 255) //dung de validate thuoc tinh cho truong name
    private String name;
    private Double price;
    private String link_img;

    private Integer type_tea;
    @Column(nullable = false,length = 100000000)
    private String infomation;

    public Integer getType_tea() {
        return type_tea;
    }

    public void setType_tea(Integer type_tea) {
        this.type_tea = type_tea;
    }

    public Tea(String name, Double price, String link_img, Integer type_tea, String infomation) {
        this.name = name;
        this.price = price;
        this.link_img = link_img;
        this.type_tea = type_tea;
        this.infomation = infomation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getLink_img() {
        return link_img;
    }

    public void setLink_img(String link_img) {
        this.link_img = link_img;
    }

    public String getInfomation() {
        return infomation;
    }

    public void setInfomation(String infomation) {
        this.infomation = infomation;
    }

    public Tea() {
    }

}
