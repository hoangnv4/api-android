package com.example.demo.Models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity // dung de khai bao khoa chinh

public class Orders {
    @Id  // dung de khai bao khoa chinh
    @GeneratedValue(strategy = GenerationType.AUTO)  // dung de khai bao khoa chinh tu sinh
    private Long id;
    private Long idTea;

    private Long idUser;

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    private String adress;

    private Long phoneNumber;

    public Orders() {
    }

    public Orders(Long idTea,Long idUser, String adress, Long phoneNumber) {
        this.idTea = idTea;
        this.idUser = idUser;
        this.adress = adress;
        this.phoneNumber = phoneNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdTea() {
        return idTea;
    }

    public void setIdTea(Long idTea) {
        this.idTea = idTea;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}
