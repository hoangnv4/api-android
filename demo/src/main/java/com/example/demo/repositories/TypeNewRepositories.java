package com.example.demo.repositories;

import com.example.demo.Models.Product;
import com.example.demo.Models.TypeNew;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TypeNewRepositories extends JpaRepository<TypeNew,Long> {
    List<TypeNew> findByName(String name);
}
