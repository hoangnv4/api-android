package com.example.demo.repositories;

import com.example.demo.Models.News;
import com.example.demo.Models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

public interface NewRepositories extends JpaRepository<News,Long> {
    List<News> findByTitle(String title);
    List<News> findByType(Integer type);

//    @Modifying
//    @Transactional
//    @Query("select * from NewRepositories where created_at >= :from and created_at <= :to ")
//    List<News> findByCreatedBetween(@Param("from") Date from, @Param("to") Date to);
}

