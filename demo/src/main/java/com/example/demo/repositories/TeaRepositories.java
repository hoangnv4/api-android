package com.example.demo.repositories;

import com.example.demo.Models.Product;
import com.example.demo.Models.Tea;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TeaRepositories extends JpaRepository<Tea,Long> {
    List<Tea> findByName(String name);
}
