package com.example.demo.repositories;

import com.example.demo.Models.Product;
import jdk.dynalink.linker.LinkerServices;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepositories extends JpaRepository<Product, Long> // product la model can dung,Long la type cua truong khoa chinh
{
    List<Product> findByName(String name);
}
