package com.example.demo.repositories;

import com.example.demo.Models.Product;
import com.example.demo.Models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepositories extends JpaRepository<User,Long> {
    User findByName(String name);
}
